// 智能提示的第二种方式
import { defineConfig } from 'vitepress'
// 解析demo
import {
  componentPreview,
  containerPreview,
} from '@vitepress-demo-preview/plugin'

export default defineConfig({
  // 2. 网站标题
  title: 'VitePress',
  // 3. 网站介绍
  description: 'Just playing around.',
  // 4. 主题配置类型
  themeConfig: {
    logo: '../images/R-D.jpg',
    // 导航菜单项的配置
    nav: [
      { text: '指南', link: '/guide/design' },
      { text: '组件', link: '/components/button' },
    ],
    // 侧边栏菜单项的配置
    sidebar: {
      // 指南
      '/guide/': [
        {
          text: '指南',
          items: [{ text: '设计', link: '/guide/design' }],
        },
      ],
      // 组件
      '/components/': [
        {
          text: '基础控件',
          items: [
            { text: 'Button 按钮', link: '/components/button' },
            { text: 'Border 按钮', link: '/components/border' },
          ],
        },
      ],
    },
    // 自定义右侧边栏的标题（在大纲链接的顶部）。 这在用另一种语言编写文档时很有用
    outlineTitle: 'In hac pagina',
    // logo 接受路径字符串或包含亮/暗模式不同logo的对象{ light: Image; dark: Image }
    // logo: '/logo.svg',
    // 替换导航中的默认站点标题
    siteTitle: '个人文档',
    // 可以定义此选项以在导航中展示带有图标的社交帐户链接
    socialLinks: [{ icon: 'github', link: 'https://gitee.com/xiao-hui-ya' }],
    // 页脚配置
    footer: {
      message: 'Released under the MIT License.',
      copyright: 'Copyright © 2019-present Evan You',
    },
    // 编辑链接可让您显示链接以编辑 Git 管理服务（例如 GitHub 或 GitLab）上的页面
    editLink: {
      pattern: 'https://github.com/vuejs/vitepress/edit/main/docs/:path',
      text: 'Edit this page on GitHub',
    },
    // 显示上次更新时间之前的前缀文本
    themeConfig: {
      lastUpdatedText: 'Updated Date',
    },
    // 一个选项用来展示 Carbon Ads
    carbonAds: {
      code: 'your-carbon-code',
      placement: 'your-carbon-placement',
    },
    // 可用于自定义出现在上一个和下一个链接上方的文本。 如果不是用英语编写文档，这很有帮助
    docFooter: {
      prev: 'Pagina prior',
      next: 'Proxima pagina',
    },
  },
  // markdowm配置
  markdown: {
    theme: {
      light: 'vitesse-light',
      dark: 'vitesse-dark',
    },
    lineNumbers: true,
    config(md) {
      md.use(componentPreview)
      md.use(containerPreview)
    },
  },
})
