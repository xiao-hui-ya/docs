import DefaultTheme from 'vitepress/theme'
// 引入饿了吗
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 图标
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// 引入markdowm解析
// import theme from 'vitepress/dist/client/theme-default/index'
import { AntDesignContainer } from '@vitepress-demo-preview/component'
import '@vitepress-demo-preview/component/dist/style.css'
// 属性组件
import Props from '../../common/Props.vue'

export default {
  ...DefaultTheme,
  // ...theme,
  enhanceApp({ app }) {
    app.use(ElementPlus)
    // Demo组件
    app.component('demo-preview', AntDesignContainer)
    // 属性组件
    app.component('Props', Props)
    // 图标
    for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
      app.component(key, component)
    }
  },
}
