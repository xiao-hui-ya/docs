# Button 按钮

## Basic

<preview path="../examples/Button/base.vue" title="按钮" description="描述文本" />

## Button API

### Button 属性

<Props :props="[
{ id: 0, name: 'size', explain: '尺寸', type: 'enum', default: '__', enum: 'large | small | default' },
{ id: 1, name: 'type', explain: '类型', type: 'enum', default: '__', enum: 'large | small | default' },
{ id: 2, name: 'plain', explain: '是否为朴素按钮', type: 'boolean', default: false },
]"/>
